<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"website\HomeController@index")->name('home');

Route::get('/login',"website\AuthController@loginShow")->name('login')->middleware('guest:web');
Route::post('/login',"website\AuthController@loginPost")->name('login.store');
Route::get('/logout',"website\AuthController@logout")->name('logout')->middleware('auth:web');

Route::middleware('auth:web')->group(function () {
    Route::get('home',"website\HomeController@index")->name('home');
});


Route::get('/admin/login',"Admin\AuthController@loginShow")->name('admin.login')->middleware('guest:admin');
Route::post('/admin/login',"Admin\AuthController@loginPost")->name('admin.login.store');
Route::get('/admin/logout',"Admin\AuthController@logout")->name('admin.logout')->middleware('auth:admin');


Route::get('/forgotpassword/{type}',            "ForgetPasswordResetController@viewForgotpassword")                 ->name('forgotpassword');
Route::get('/resetPassword/{email_encode}',     "ForgetPasswordResetController@ResetPassword")                      ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  "ForgetPasswordResetController@SetNewPassword")                     ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  "ForgetPasswordResetController@AjaxCallForForgotPasswordEmail")     ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

Route::get('register',"website\RegisterController@viewRegister")->name("register");
Route::post('register',"website\RegisterController@registerUser")->name("registerUser");



Route::middleware('auth:admin')->group(function () {
    Route::get('admin/home',"Admin\HomeController@index")->name('admin.home');
});

